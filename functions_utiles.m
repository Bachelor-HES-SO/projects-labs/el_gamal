j = 0;

# On utilise floor pour retourner la division arrondie à l'entier inférieur.
function retval = modulo(x, y)
  retval = x - y .* floor(x ./ y);
endfunction

# Calcul du pgcd entre a et b selon l'algorithme d'Euclide.
function retval = pgcd(a, b)
  if (b == 0)
    retval = a;
  else
    retval = pgcd(b, modulo(a, b));
  endif
endfunction

# Cette fonction contient un tableau contenant tous les nombres premiers à trois chiffres.
# Elle pioche de manière aléatoire dedans (randi) et retourne le nombre trouvé.
function retval = premier()
  tab = [101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,929,937,941,947,953,967,971,977,983,991,997];
  retval = tab(randi(length(tab)));
endfunction

# Cette fonction retourne le nombre suivant : (n^m) mod p 
# en le calculant ainsi : (n mod p) * (n mod p) * ... * (n mod p), le tout m fois.
function retval = expo_mod(n, m, p)
  if (m == 0)
    res = 1;
  else
    res = modulo(n, p);
    
    for i = 2 : m
      res = res * modulo(n, p);
      res = modulo(res, p);
    endfor
  endif

  retval = res;
endfunction

function retval = generateur(p)
  trouve = false;
  i = floor(p / 2);
  while (!trouve)
    tab = zeros(1, p);
    j = 0;
    reste_existe = false;
    do
      val = expo_mod(i, j, p);
      if (tab(val) == 0)
        tab(val) = val;
      else
        existe = true;
      endif
      j++;
    until (reste_existe || j == p-1)
    if (!reste_existe)
      trouve = true;
    endif
    i++;
  endwhile
  retval = i - 1;
endfunction

# Steven
function retval = euclide_etu(q, r)
  original_r = r;
  T = [0, 0];
  if (pgcd(q, r) == 1)
    Q = [1, 0];
    R = [0, 1];
    part = floor(q/r);
    modu = modulo(q, r);
    T = Q - part * R;

    while(modu != 1)
      q = r;
      r = modu;
      modu = modulo(q, r);
      Q = R;
      part = floor(q/r);
      R = T;
      T = Q - part * R;
    endwhile
    if (T(1) < 0)
      T(1) = T(1) + original_r;
    endif
  endif
  retval = T(1);
endfunction

# Steven
function retval = signature(p, a, alpha, M)
  do
    k = randi(p-2);
  until(pgcd(k, p-1) == 1)
  gamma = expo_mod(alpha, k, p)
  k_inverse = euclide_etu(k, p-1);
  delta = modulo((M - a * gamma) * k_inverse, p - 1)
  retval = [gamma, delta];
endfunction

function retval = verification(M, gamma, delta, p, alpha, beta)
  retval = modulo(expo_mod(beta, gamma, p) * expo_mod(gamma, delta, p), p) == expo_mod(alpha, M, p);
endfunction

#a = generateur(599)
#g = expo_mod(179, 17, 132)
#b = euclide_etu(789, 566)

M = 42
p = premier()
a = randi(p-1) + 1
alpha = generateur(p)
beta = expo_mod(alpha, a, p)
sign_el_gamal = signature(p, a, alpha, M)
verif_el_gamal = verification(M, sign_el_gamal(1), sign_el_gamal(2), p, alpha, beta)

#gmla = signature(11, 7, 2, 42)