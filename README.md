#   Signature El Gamal

> Prof Noria Foukia | Raed Abdennadher et Steven Liatti | HEPIA 2016-2017
>

![octave](./docs/octave-logo.png)

# Introduction

Dans ce travail pratique, nous avons implémenté la méthode de signature El Gamal. Nous avons utilisé le langage Octave. Chaque fonction écrite sera détaillée par la suite.

# Fonctions de base

Ces trois fonctions font office de « briques de base ».

## Modulo

On utilise la fonction `floor` pour retourner la division arrondie à l'entier inférieur. Attends deux paramètres, `x` et `y`. La fonction accomplit l’opération.

```octave
function retval = modulo(x, y)
 retval = x - y .* floor(x ./ y);
endfunction
```

## Plus grand commun diviseur

Calcul du plus grand commun diviseur entre `a` et `b` selon l'algorithme d'Euclide.

```octave
function retval = pgcd(a, b)
 if (b == 0)
  retval = a;
 else
  retval = pgcd(b, modulo(a, b));
 endif
endfunction
```

## Générateur de nombre premier à trois chiffres

Cette fonction contient un tableau contenant tous les nombres premiers à trois chiffres. Elle pioche de manière aléatoire dedans (`randi`) et retourne le nombre trouvé.

```octave
function retval = premier()
 tab = [101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,929,937,941,947,953,967,971,977,983,991,997];
 retval = tab(randi(length(tab)));
endfunction
```

# Fonctions d’arithmétique modulaire

## Exponentiation modulaire

Cette fonction permet de calculer l’exponentiation modulaire de $`n^m`$ en calculant :

![](./docs/formule1.png)

 ```octave
 function retval = expo_mod(n, m, p)
  if (m == 0)
   res = 1;
  else
   res = modulo(n, p);
   for i = 2 : m
    res = res * modulo(n, p);
    res = modulo(res, p);
   endfor
  endif
  retval = res;
 endfunction
 ```

## Générateur

Cette fonction permet de calculer un générateur de `p` en appliquant la logique suivante :

1. Prendre un nombre entier `i` égal à $`\left [ \frac{p}{2} \right ]`$

2. Initialiser à `0` un tableau `tab` de `p` cases

3. Calculer $`val = i^j mod (p)`$ avec l’exponentiation modulaire (`j` allant de `0` à `p-1`)

4. Enregistrer `val` dans le tableau `tab` à la case `tab[val]`. Si la valeur de cette case est différente de `0` (ça veut dire que le reste est déjà obtenu par un autre `j`) alors on quitte la boucle (via la variable booléenne `reste_existe = true`) et on avance `i` d’un pas (`i++`)

5. Si tout s’est bien passé, et qu’on a quitté la boucle avec `reste_existe = false`, on renvoie `i` qui sera le générateur de `p`

```octave
function retval = generateur(p)
 trouve = false;
 i = floor(p / 2);
 while (!trouve)
  tab = zeros(1, p);
  j = 0;
  reste_existe = false;
  do
   val = expo_mod(i, j, p);
   if (tab(val) == 0)
    tab(val) = val;
   else
    existe = true;
   endif
   j++;
  until (reste_existe || j == p-1)
  if (!reste_existe)
   trouve = true;
  endif
  i++;
 endwhile
 retval = i - 1;
endfunction
```

## Coefficients de Bézout avec Euclide étendu

Cette fonction applique l’algorithme d’Euclide (étendu) pour obtenir les coefficients de Bézout, comme vu en cours. Elle permet de trouver l’inverse de `q` modulo `r` : $`q^{-1}\;mod\;r`$. 

Dans le code :

* `part` vaut la valeur suivante : $`\left [ \frac{p}{r} \right ]`$

* `modu` = $`q\;mod\;r`$

```octave
function retval = euclide_etu(q, r)
 original_r = r;
 T = [0, 0];
 if (pgcd(q, r) == 1)
  Q = [1, 0];
  R = [0, 1];
  part = floor(q/r);
  modu = modulo(q, r);
  T = Q - part * R;
  while(modu != 1)
   q = r;
   r = modu;
   modu = modulo(q, r);
   Q = R;
   part = floor(q/r);
   R = T;
   T = Q - part * R;
  endwhile
  if (T(1) < 0)
   T(1) = T(1) + original_r;
  endif
 endif
 retval = T(1);
endfunction
```

# Fonctions de la signature El Gamal

##  Signature

Fonction permettant de chiffrer un message `M` grâce à un `k` et à la clé privée. Repose sur le principe de générer un nombre premier `p` suffisamment grand pour que le problème du logarithme discret devienne difficile dans $`\mathbb{Z}^*p`$ . Elle génère un couple $`\gamma`$ et $`\delta`$ tels que :

$`\gamma = \alpha^k \; mod \; q \;\;\;\;\;\;\;\;\; \delta = \left ( M - \alpha\gamma \right ) * k^{-1} \; mod \left( p-1 \right )`$

```octave
function retval = signature(p, a, alpha, M)
 do
  k = randi(p-2);
 until(pgcd(k, p-1) == 1)
 gamma = expo_mod(alpha, k, p)
 k_inverse = euclide_etu(k, p-1);
 delta = modulo((M - a ` gamma) ` k_inverse, p - 1)
 retval = [gamma, delta];
endfunction 
```

## Vérification de la signature

Cette fonction permet de vérifier la signature d’un message envoyé avec El-Gamal. Elle renvoie 1 si la vérification est positive, et `0` sinon. L’écriture mathématique de la vérification El-Gamal :

$`\beta^\gamma.\gamma^\delta \; mod \; p \; \equiv ?\; \alpha^M \; mod \; p`$

Donc, puisque nous avons implémenté toutes les fonctions nécessaires, le corps de cette fonction devient réduit en une seule ligne de code.

```octave
function retval = verification(M, gamma, delta, p, alpha, beta)
 retval = modulo(expo_mod(beta, gamma, p) * expo_mod(gamma, delta, p), p) == expo_mod(alpha, M, p);
endfunction
```

# Exemple d’utilisation

En prenant ces paramètres :

```octave
M = 42
p = premier()
a = randi(p-1) + 1
alpha = generateur(p)
beta = expo_mod(alpha, a, p)
sign_el_gamal = signature(p, a, alpha, M)
verif_el_gamal = verification(M, sign_el_gamal(1), sign_el_gamal(2), p, alpha, beta)
```

On obtient le résultat suivant :

```octave
M = 42
p = 751
a = 252
alpha = 375
beta = 545
gamma = 202
delta = 66
sign_el_gamal =
  202  66
verif_el_gamal = 1
```

